package platform

import "testing"

func TestAuthorizer(t *testing.T) {
	var p Platform
	p.config.AzureAuth.AZURE_CLIENT_SECRET = "J5X.82-ENvz8WBJHy.b81V~6pXi-hNb1.U"
	p.config.AzureAuth.AZURE_CLIENT_ID = "b3564ca6-89fa-4d7b-b651-7dda1df695d3"
	p.config.AzureAuth.AZURE_TENANT_ID = "0e3e2e88-8caf-41ca-b4da-e3b33b6c52ec"
	credError := p.GetCredentials()
	if credError != nil {
		t.Error("Error getting Azure Authorizer.")
	}

}
