package platform

import (
	"context"
	"fmt"

	"github.com/Azure/go-autorest/autorest"
	"github.com/hashicorp/waypoint-plugin-examples/template/registry"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
)

type DeployConfig struct {
	Region     string
	AzureAuth  AuthType
	AppService AppServicePlan
	Function   FunctionConfig
}

type FunctionConfig struct {
	FunctionName       string
	StorageAccount     string
	DisableAppInsights bool
	OsType             string
	RunTime            string
	FunctionVersion    int
}

type AppServicePlan struct {
	AppServicePlan string
	StorageAccount string
	ResourceGroup  string
	SKU            string
}

type AuthType struct {
	AZURE_CLIENT_ID     string `hcl:client_id`
	AZURE_TENANT_ID     string
	AZURE_CLIENT_SECRET string
	AZURE_SUBSCRIPTION  string
	AzureAuthorizer     *autorest.Authorizer
}

type Platform struct {
	config DeployConfig
}

// Implement Configurable
func (p *Platform) Config() (interface{}, error) {
	return &p.config, nil
}

// Implement ConfigurableNotify
func (p *Platform) ConfigSet(config interface{}) error {
	c, ok := config.(*DeployConfig)
	if !ok {
		// The Waypoint SDK should ensure this never gets hit
		return fmt.Errorf("Expected *DeployConfig as parameter")
	}

	// validate the config
	if c.Region == "" {
		return fmt.Errorf("Region must be set to a valid directory")
	}

	return nil
}

// Implement Builder
func (p *Platform) DeployFunc() interface{} {
	// return a function which will be called by Waypoint
	return p.deploy
}

// A BuildFunc does not have a strict signature, you can define the parameters
// you need based on the Available parameters that the Waypoint SDK provides.
// Waypoint will automatically inject parameters as specified
// in the signature at run time.
//
// Available input parameters:
// - context.Context
// - *component.Source
// - *component.JobInfo
// - *component.DeploymentConfig
// - *datadir.Project
// - *datadir.App
// - *datadir.Component
// - hclog.Logger
// - terminal.UI
// - *component.LabelSet

// In addition to default input parameters the registry.Artifact from the Build step
// can also be injected.
//
// The output parameters for BuildFunc must be a Struct which can
// be serialzied to Protocol Buffers binary format and an error.
// This Output Value will be made available for other functions
// as an input parameter.
// If an error is returned, Waypoint stops the execution flow and
// returns an error to the user.
func (b *Platform) deploy(ctx context.Context, ui terminal.UI, artifact *registry.Artifact) {
	u := ui.Status()
	defer u.Close()
	u.Update("Deploy application")

	//return &Deployment{}, nil
}
