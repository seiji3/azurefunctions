package platform

import (
	"context"

	azureweb "github.com/Azure/azure-sdk-for-go/services/web/mgmt/2020-06-01/web"
)

type SKUType int

var SKUArray []string = []string{
	"B1", "B2", "B3", "D1", "F1", "FREE", "I1", "I2", "I3", "P1V2", "P1V3", "P2V2", "P2V3", "P3V2", "P3V3", "PC2", "PC3", "PC4", "S1", "S2", "S3", "SHARED",
}

func IsValidRuntime(element string) SKUType {

	for k, v := range SKUArray {
		if element == v {
			var runtimeIndex SKUType = SKUType(k)
			return runtimeIndex
		}
	}
	return -1 //not found.
}

func (p *Platform) ListAppServicePlans(ctx context.Context) (*azureweb.AppServicePlanCollectionPage, error) {
	client := azureweb.NewAppServicePlansClient(p.config.AzureAuth.AZURE_SUBSCRIPTION)
	client.Authorizer = *p.config.AzureAuth.AzureAuthorizer
	result, resultError := client.List(ctx, nil)
	if resultError != nil {
		return nil, resultError
	}
	return &result, nil

}

func (p *Platform) CreateAppServicePlan(ctx context.Context) error {
	client := azureweb.NewAppServicePlansClient(p.config.AzureAuth.AZURE_SUBSCRIPTION)
	client.Authorizer = *p.config.AzureAuth.AzureAuthorizer
	var plan azureweb.AppServicePlan
	plan.Location = &(p.config.Region)
	//plan.ResourceGroup = &(p.config.AppService.ResourceGroup)
	var sku azureweb.SkuDescription
	sku.Name = &(p.config.AppService.SKU)
	plan.Sku = &sku
	_, resultError := client.CreateOrUpdate(ctx, p.config.AppService.ResourceGroup, p.config.AppService.AppServicePlan, plan)
	if resultError != nil {
		return nil
	}
	return nil
}

func (p *Platform) AppServiceExists(ctx context.Context) (*bool, error) {
	result, resultError := p.ListAppServicePlans(ctx)
	if resultError != nil {
		return nil, resultError
	}
	var flag bool
	values := result.Values()
	for i := 0; i < len(values); i++ {
		if values[i].Name == &p.config.AppService.AppServicePlan {
			flag = true
			return &flag, nil
		}

	}
	flag = false
	return &flag, nil
}
