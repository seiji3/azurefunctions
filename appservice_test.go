package platform

import (
	"context"
	"fmt"
	"testing"
)

func TestListServicePlan(t *testing.T) {
	var p Platform
	p.config.AzureAuth.AZURE_CLIENT_SECRET = "aaa"
	p.config.AzureAuth.AZURE_CLIENT_ID = "yyy"
	p.config.AzureAuth.AZURE_TENANT_ID = "xxx"
	credError := p.GetCredentials()
	if credError != nil {
		t.Error("Error getting Azure Authorizer.")
	}
	p.config.AppService.AppServicePlan = "WayPointServicePlanTest (P1v2: 1)"
	p.config.AppService.ResourceGroup = "WayPointResourceGroup"
	p.config.AzureAuth.AZURE_SUBSCRIPTION = "02d0e06b-ed9d-4ca5-bb9f-0a0243a9c9f2"
	appserviceplan, _ := p.ListAppServicePlans(context.Background())
	fmt.Println(appserviceplan)
}

func TestServicePlanCreation(t *testing.T) {
	var p Platform
	p.config.AzureAuth.AZURE_CLIENT_SECRET = "aaa"
	p.config.AzureAuth.AZURE_CLIENT_ID = "bbb"
	p.config.AzureAuth.AZURE_TENANT_ID = "ccc"
	credError := p.GetCredentials()
	if credError != nil {
		t.Error("Error getting Azure Authorizer.")
	}
	p.config.Region = "East US"
	p.config.AppService.AppServicePlan = "GoTest"
	p.config.AppService.ResourceGroup = "WayPointResourceGroup"
	p.config.AzureAuth.AZURE_SUBSCRIPTION = "02d0e06b-ed9d-4ca5-bb9f-0a0243a9c9f2"
	p.config.AppService.SKU = "P1V2"
	err := p.CreateAppServicePlan(context.Background())
	if err != nil {
		t.Errorf("Error: %v", err)
	}
}
