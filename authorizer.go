package platform

import "github.com/Azure/go-autorest/autorest/azure/auth"

func (p *Platform) GetCredentials() error {

	authClient := auth.NewClientCredentialsConfig(p.config.AzureAuth.AZURE_CLIENT_ID, p.config.AzureAuth.AZURE_CLIENT_SECRET, p.config.AzureAuth.AZURE_TENANT_ID)
	authorizier, errAuthorizer := authClient.Authorizer()

	if errAuthorizer != nil {
		return errAuthorizer
	}

	p.config.AzureAuth.AzureAuthorizer = &authorizier
	return nil

}
