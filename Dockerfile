FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS installer-env

RUN apt-get update
	
RUN echo Y | apt-get install ca-certificates curl apt-transport-https lsb-release gnupg

RUN curl -sL https://aka.ms/InstallAzureCLIDeb | bash

RUN curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg

RUN mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg

RUN sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/debian/$(lsb_release -rs | cut -d'.' -f 1)/prod $(lsb_release -cs) main" > /etc/apt/sources.list.d/dotnetdev.list'

RUN apt-get update
RUN apt-get install azure-functions-core-tools-3
WORKDIR /mnt

