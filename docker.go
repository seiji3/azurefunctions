package platform

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"unicode/utf8"
)

func CreateDocker() *error {

	docker := `FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS installer-env

RUN apt-get update
	
RUN echo Y | apt-get install ca-certificates curl apt-transport-https lsb-release gnupg

RUN curl -sL https://aka.ms/InstallAzureCLIDeb | bash

RUN curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg

RUN mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg

RUN sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/debian/$(lsb_release -rs | cut -d'.' -f 1)/prod $(lsb_release -cs) main" > /etc/apt/sources.list.d/dotnetdev.list'

RUN apt-get update
RUN apt-get install azure-functions-core-tools-3
WORKDIR /mnt

RUN ./publish_function.sh`
	dockerError := ioutil.WriteFile("Dockerfile", []byte(docker), 0644)
	if dockerError != nil {
		return &dockerError
	}
	return nil
}

type RunningDockerId struct {
	ID    string
	Image string
}

func GetDockerImageId() (*string, error) {

	cmd, cmdError := exec.Command("docker", "ps", "--filter", "name=waypoint-instance", "--format", "'{{json . }}").Output()
	if cmdError != nil {
		fmt.Println(cmdError)
		return nil, cmdError
	}
	fmt.Println(string(cmd))
	formatted := trimFirstRune(string(cmd))
	var id RunningDockerId
	json.Unmarshal([]byte(formatted), &id)
	return &id.ID, nil
}

func InstantiateDockerContainer() error {
	cwd, cwdError := os.Getwd()
	if cwdError != nil {
		return cwdError
	}
	mount := cwd + ":/mnt"
	_, outputError := exec.Command("docker", "run", "-d", "-v", mount, "--name", "waypoint-instance", "waypoint-azure", "tail", "-f", "/dev/null").Output()
	if outputError != nil {
		return outputError
	}
	return nil
}

func trimFirstRune(s string) string {
	_, i := utf8.DecodeRuneInString(s)
	return s[i:]
}

func (p *Platform) ContainerAzLogin() error {
	imageId, imageError := GetDockerImageId()
	if imageError != nil {
		return imageError
	}
	output, outputError := exec.Command("docker", "exec", "-it", *imageId, "az", "login", "--service-principal", "--username", p.config.AzureAuth.AZURE_CLIENT_ID, "--password", p.config.AzureAuth.AZURE_CLIENT_SECRET, "--tenant", p.config.AzureAuth.AZURE_TENANT_ID).Output()
	if outputError != nil {
		return outputError
	}
	return nil
}

func (p *Platform) PublishFunction() error {
	imageId, imageError := GetDockerImageId()
	if imageError != nil {
		return imageError
	}

	function, funcError := exec.Command("docker", "exec", "-it", *imageId, "func", "azure", p.config.Function.FunctionName, "publish", p.config.AppService.AppServicePlan).Output()
	if funcError != nil {
		return funcError
	}
	fmt.Println(function)
	return nil
}

func (p *Platform) CreateFunction() error {
	imageId, imageError := GetDockerImageId()
	if imageError != nil {
		return imageError
	}
	output, outputError := exec.Command("docker", "exec", "-it", *imageId, "az", "functionapp", "create", "--name", p.config.Function.FunctionName, "--resource-group", p.config.AppService.ResourceGroup, "--plan", p.config.AppService.AppServicePlan, "--disable-app-insights", strconv.FormatBool(p.config.Function.DisableAppInsights), "--os-type", p.config.Function.OsType, "--runtime", p.config.Function.RunTime, "--functions-version", strconv.Itoa(p.config.Function.FunctionVersion)).Output()
	if outputError != nil {
		return outputError
	}
	fmt.Println(string(output))
	return nil
}

func BuildDockerContainer() error {
	output, outputError := exec.Command("docker", "build", ".", "--tag", "waypoint-azure").Output()
	if outputError != nil {
		return outputError
	}
	fmt.Println(string(output))
	return nil
}

func KillDockerContainer() error {
	_, stopError := exec.Command("docker", "stop", "waypoint-azure").Output()
	if stopError != nil {
		return stopError
	}
	_, rmError := exec.Command("docker", "rm", "waypoint-instance").Output()
	if rmError != nil {
		return rmError
	}
	return nil
}
