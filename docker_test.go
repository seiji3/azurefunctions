package platform

import (
	"fmt"
	"testing"
)

func TestDockerCreation(t *testing.T) {
	errDocker := CreateDocker()
	if errDocker != nil {
		t.Fatal("Dockerfile could not be created")
	}
}

func TestDockerBuild(t *testing.T) {
	result := BuildDockerContainer()
	if result != nil {
		t.Fatal("Dockerfile could not be created")
	}
}

func TestRunContainer(t *testing.T) {

	result := InstantiateDockerContainer()
	if result != nil {
		t.Fatal("Dockerfile could not be created")
	}
}

func TestGetDockerImageId(t *testing.T) {
	id, idError := GetDockerImageId()
	if idError != nil {
		t.Errorf("Error retrieving docker image id for waypoint-azure")
	}
	fmt.Println(id)

}
