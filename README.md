# Service Principal Creation

To create a Service Principal follow [this](https://docs.microsoft.com/en-us/azure/active-directory/develop/howto-create-service-principal-portal)

To create a service principal in Azure AD run the following AZ CLI command:

az ad sp create-for-rbac --name <service principal name> --create-cert

This will generate an App ID, Tenant ID and a valid certificate for authentication

## Auth Methods

1. **Client Credentials**: Azure AD Application ID and Secret.

    - `AZURE_TENANT_ID`: Specifies the Tenant to which to authenticate.
    - `AZURE_CLIENT_ID`: Specifies the app client ID to use.
    - `AZURE_CLIENT_SECRET`: Specifies the app secret to use.

2. **Client Certificate**: Azure AD Application ID and X.509 Certificate.

    - `AZURE_TENANT_ID`: Specifies the Tenant to which to authenticate.
    - `AZURE_CLIENT_ID`: Specifies the app client ID to use.
    - `AZURE_CERTIFICATE_PATH`: Specifies the certificate Path to use.
    - `AZURE_CERTIFICATE_PASSWORD`: Specifies the certificate password to use.

3. **Resource Owner Password**: Azure AD User and Password. This grant type is *not
   recommended*, use device login instead if you need interactive login.

    - `AZURE_TENANT_ID`: Specifies the Tenant to which to authenticate.
    - `AZURE_CLIENT_ID`: Specifies the app client ID to use.
    - `AZURE_USERNAME`: Specifies the username to use.
    - `AZURE_PASSWORD`: Specifies the password to use.

4. **Azure Managed Service Identity**: Delegate credential management to the
   platform. Requires that code is running in Azure, e.g. on a VM. All
   configuration is handled by Azure. See [Azure Managed Service
   Identity](https://docs.microsoft.com/azure/active-directory/msi-overview)
   for more details.

# To deploy a function

## Prerequisites

1. App Service Plan

This defines the environment of the function that is to run and how much compute power that is backing the function.  Tags can be defined as well with the app service plan although that code has not been written.

2. Storage Account

A "properly" configured storage account needs to be provided as input other wise the actual function creation will error out.  The storage account needs to have an endpoint either to a File, Table, Blob or Queue.

3. FunctionApp 

A FunctionApp object needs to be created.  This ties all of the above information together and creates the logical container to host the application.

4. Deploying Function

After all the above prep work, we finally push the function out.  There is a concept of "slots" that allows for Blue / Green deployment of applications.  I will try to research this a bit further and write code around it.