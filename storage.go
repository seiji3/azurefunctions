package platform

import (
	"context"

	"github.com/Azure/azure-sdk-for-go/services/storage/mgmt/2019-06-01/storage"
)

func (p *Platform) CreateStorageAccount(ctx context.Context) {
	client := storage.NewAccountsClient(p.config.AzureAuth.AZURE_SUBSCRIPTION)
	client.Authorizer = *p.config.AzureAuth.AzureAuthorizer
	//var param storage.AccountCreateParameters

}
